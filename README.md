# [OdooInstall](https://www.voducthang.com "Vo Duc Thang's Homepage") Install Script

## Requirement
 - Ubuntu 22.04 or Latest<br/>

## Installation procedure

##### 1. Download the script:
```
git clone https://gitlab.com/ducthangict.dhtn/install_odoo_ubuntu.git -b main
cd install_odoo_ubuntu

```
##### 2. Modify the parameters as you wish.
 - Open odoo_install.sh modifier if you want
 ```
    OE_USER="odoo"
    OE_HOME="/$OE_USER"
    OE_HOME_EXT="/$OE_USER/${OE_USER}-server"
    # The default port where this Odoo instance will run under (provided you use the command -c in the terminal)
    # Set to true if you want to install it, false if you don't need it or have it already installed.
    INSTALL_WKHTMLTOPDF="True"
    # Set the default Odoo port (you still have to use -c /etc/odoo-server.conf for example to use this.)
    OE_PORT="8069"
    # Choose the Odoo version which you want to install. For example: 16.0, 15.0, 14.0 or saas-22. When using 'master' the master version will be installed.
    # IMPORTANT! This script contains extra libraries that are specifically needed for Odoo 16.0
    OE_VERSION="13.0"
    # Set this to True if you want to install the Odoo enterprise version!
    IS_ENTERPRISE="False"
    # Installs postgreSQL V16 instead of defaults (e.g V12 for Ubuntu 20/22) - this improves performance
    INSTALL_POSTGRESQL_SIXTEEN="True"
    # Set this to True if you want to install Nginx!
    INSTALL_NGINX="True"
    # Set the superadmin password - if GENERATE_RANDOM_PASSWORD is set to "True" we will automatically generate a random password, otherwise we use this one
    OE_SUPERADMIN="admin"
    # Set to "True" to generate a random password, "False" to use the variable in OE_SUPERADMIN
    GENERATE_RANDOM_PASSWORD="True"
    OE_CONFIG="${OE_USER}-server"
    # Set the website name
    WEBSITE_NAME="_"
    # Set the default Odoo longpolling port (you still have to use -c /etc/odoo-server.conf for example to use this.)
    LONGPOLLING_PORT="8072"
    # Set to "True" to install certbot and have ssl enabled, "False" to use http
    ENABLE_SSL="True"
    # Provide Email to register ssl certificate
    ADMIN_EMAIL="admin@vms.io.vn"
 ```
 - Need to change if you enable NGINX install and SSL
 ```
    WEBSITE_NAME="abc.com.vn"
    ADMIN_EMAIL="admin@abc.com.vn"
    with abc.com.vn == your domain
 ```
#### 3. Make the script executable
```
sudo chmod +x odoo_install.sh
```
##### 4. Execute the script:
```
./odoo_install.sh
```

##### 5. Check services on system:
```
ps -ef | grep odoo
```
If it's not exist please start it with commandline:
```
/etc/init.d/odoo-server start
```
##### 6. Fix error core Odoo return bug

```
    # DIR
    /odoo/odoo-server/odoo/service/server.py

    def check_limits(self):
        ...
        # resource.setrlimit(resource.RLIMIT_CPU, (cpu_time + config['limit_time_cpu'], hard))
        # Replace it to -> 
        resource.setrlimit(resource.RLIMIT_CPU, (int(cpu_time + config['limit_time_cpu']), hard))
```

## Contributor
Name: Vo Duc Thang <br/>
Phone: 0989 464 344 <br/>
Website: https://voducthang.com <br/>
Skype: ictlucky.dhtn <br/>
